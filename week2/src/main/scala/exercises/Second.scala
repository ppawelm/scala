package exercises

object Second extends App {

    class Rational(x: Int, y: Int) {
        require(y > 0, "Denominator must be positive")

        def this(x: Int) = this(x, 1)

        private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

        val numer: Int = x / gcd(x, y)

        val denom: Int = y / gcd(x, y)

        def add(r: Rational): Rational = new Rational(this.numer * r.denom + this.denom * r.numer, this.denom * r.denom)

        def sub(r: Rational): Rational = add(r.neg)

        def neg: Rational = new Rational(-this.numer, this.denom)

        def less(r: Rational): Boolean = this.numer * r.denom < r.numer * this.denom

        def max(r: Rational): Rational = if (this.less(r)) r else this

        override def toString: String = this.numer + "/" + this.denom
    }

    val o: Rational = new Rational(1, 2)
    val s: Rational = new Rational(2, 3)
    println(o.add(s))
    println(o.sub(s))
    println(o.less(s))
    println(o.max(s))

    class RationalImproved(x: Int, y: Int) {
        require(y > 0, "Denominator must be positive")

        def this(x: Int) = this(x, 1)

        private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

        val numer: Int = x / gcd(x, y)

        val denom: Int = y / gcd(x, y)

        def  + (r: RationalImproved): RationalImproved =
            new RationalImproved(this.numer * r.denom + this.denom * r.numer, this.denom * r.denom)

        def  - (r: RationalImproved): RationalImproved = this + -r

        def unary_- : RationalImproved = new RationalImproved(-this.numer, this.denom)

        def < (r: RationalImproved): Boolean = this.numer * r.denom < r.numer * this.denom

        def max(r: RationalImproved): RationalImproved = if(this < r) r else this

        override def toString: String = this.numer + "/" + this.denom
    }

    val oi: RationalImproved = new RationalImproved(5, 7)
    val si: RationalImproved = new RationalImproved(2, 11)
    println(oi + si)
    println(oi + si)
    println(oi - si)
    println(oi < si)
    println(oi max si)
}
