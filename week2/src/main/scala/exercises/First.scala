package exercises

object First extends App {
    // Simple way

    def id(x: Int): Int = x

    def sumInts(a: Int, b: Int): Int =
        if (a > b) 0 else a + sumInts(a + 1, b)

    def cube(x: Int): Int = x * x * x

    def sumCubes(a: Int, b: Int): Int =
        if (a > b) 0 else cube(a) + sumCubes(a + 1, b)

    def factorial(x: Int): Int =
        if (x == 0) 1 else x * factorial(x - 1)

    def sumFactorials(a: Int, b: Int): Int =
        if (a > b) 0 else factorial(a) + sumFactorials(a + 1, b)

    println("--- simple recursion ---")
    println(sumInts(0, 5))
    println(sumCubes(0, 5))
    println(sumFactorials(0, 5))

    // High-Order functions

    def sum(f: Int => Int, a: Int, b: Int): Int =
        if (a > b) 0 else f(a) + sum(f, a + 1, b)

    def sumIntsF(a: Int, b: Int): Int = sum(id, a, b)

    def sumCubesF(a: Int, b: Int): Int = sum(cube, a, b)

    def sumFactorialsF(a: Int, b: Int): Int = sum(factorial, a, b)

    println("--- high-order functions ---")
    println(sumIntsF(0, 5))
    println(sumCubesF(0, 5))
    println(sumFactorialsF(0, 5))

    // Anonymous functions

    def sumIntsA(a: Int, b: Int): Int = sum(x => x, a, b)

    def sumCubesA(a: Int, b: Int): Int = sum(x => x * x * x, a, b)

    println("--- anonymous functions ---")
    println(sumIntsA(0, 5))
    println(sumCubesA(0, 5))

    // tail recursion sum

    def sumT(f: Int => Int, a: Int, b: Int): Int = {

        def loop(a: Int, acc: Int): Int =
            if (a > b) acc else loop(a + 1, acc + f(a))

        loop(a, 0)
    }

    println("--- tail recursion ---")
    println(sumT(x => x, 0, 5))
    println(sumT(x => x * x * x, 0, 5))
    println(sumT(factorial, 0, 5))

    // Functions currying

    def sumC(f: Int => Int): (Int, Int) => Int = {

        def sumF(a: Int, b: Int): Int =
            if (a > b) 0 else f(a) + sumF(a + 1, b)

        sumF
    }

    def sumIntsC: (Int, Int) => Int = sumC(x => x)

    def sumCubesC: (Int, Int) => Int = sumC(x => x * x * x)

    def sumFactorialsC: (Int, Int) => Int = sumC(factorial)

    println("--- currying ---")
    println(sumIntsC(0, 5))
    println(sumCubesC(0, 5))
    println(sumFactorialsC(0, 5))

    println(sumC(x => x)(0, 5))
    println(sumC(x => x * x * x)(0, 5))
    println(sumC(factorial)(0, 5))

    def sumC2(f: Int => Int)(a: Int, b: Int): Int =
        if (a > b) 0 else f(a) + sumC2(f)(a + 1, b)

    println("--- currying second version ---")
    println(sumC2(x => x)(0, 5))
    println(sumC2(x => x * x * x)(0, 5))
    println(sumC2(factorial)(0, 5))

    def product(f: Int => Int)(a: Int, b: Int): Int =
        if (a > b) 1 else f(a) * product(f)(a + 1, b)

    def fact(n: Int): Int = product(x => x)(1, n)

    println("--- More examples ---")
    println(product(x => x * x)(3, 4))
    println(fact(5))

    def mapReduce(f: Int => Int, combine: (Int, Int) => Int, unitValue: Int)(a: Int, b: Int): Int =
        if (a > b) unitValue else combine(f(a), mapReduce(f, combine, unitValue)(a + 1, b))

    def product2(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (r, l) => r * l, 1)(a, b)

    println("--- Last one ---")
    println(product2(x => x * x)(3, 4))
}
