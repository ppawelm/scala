package funsets

object Main extends App {

    import FunSets._

    val mySet = union(singletonSet(2), singletonSet(4))
    printSet(mySet)
    println(exists(mySet, x => x % 2 == 0))
    val mapSet = map(mySet, x => x - 2)
    printSet(mapSet)

    val a = union(singletonSet(1), singletonSet(3))
    val b = union(singletonSet(4), singletonSet(5))

    val testSet = union(a,b)
    printSet(testSet)
}
